const Sequelize = require('sequelize') ;
const fs = require('fs');
const path = require('path');

const { development: config } = require('../config/config.json');

let database = null;

const loadModels = (sequelize) => {
  const dir = path.join(__dirname, '../models');
  const models = [];
  fs.readdirSync(dir).forEach(file => {
    const modelDir = path.join(dir, file);
    const model = sequelize.import(modelDir);
    models[model.name] = model;
  });
  return models;
};

module.exports = () => {
  if (!database) {
    const sequelize = new Sequelize({ ...config });

    database = {
      sequelize,
      Sequelize,
      models: {},
    };

    database.models = loadModels(sequelize);

    sequelize.sync().done(() => database);

    // database.models.Product.bulkCreate([
    //   {
    //     imageSrc:'https://dgn7v532p0g5j.cloudfront.net/unsafe/products/photos/semi-environment/L_N3G.DAYO18CCa.png.1520939726027.jpeg',
    //     description:'Role extremamente top',
    //     title:'Navio submarino',
    //     price: 10,
    //     discount: 45
    //   },
    //   {
    //     imageSrc:'https://dgn7v532p0g5j.cloudfront.net/unsafe/products/photos/semi-environment/C_N3G.MONO18CHa.png.1521468936838.jpeg',
    //     description:'Lorem ipslom bla bla',
    //     title:'Cama de casal Moisés',
    //     price: 20,
    //     discount: 30
    //   },
    //   {
    //     imageSrc:'https://dgn7v532p0g5j.cloudfront.net/unsafe/products/photos/still/MUTRI.DAYO18AR.png.1519386214503.jpeg',
    //     description:'Lorem ipslom bla2 bla2',
    //     title:'Kenji de Casal',
    //     price: 30,
    //     discount: 15
    //   }
    // ]);

  }
  return database;
}
